﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gestiontransport.Models
{
    public class Client
    {
        public long idPersonne{ get; set; }
        public string adresse { get; set; }
        public string email { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string sexe { get; set; }
        public string telephone { get; set; }
        public string login { get; set; }
        public string matricule { get; set; }
        public string password { get; set; }
    }
}